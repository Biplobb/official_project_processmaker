#!/bin/sh
echo $PATH | egrep "/home/biplob/Documents/processmakers/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/home/biplob/Documents/processmakers/sqlite/bin:/home/biplob/Documents/processmakers/php/bin:/home/biplob/Documents/processmakers/mysql/bin:/home/biplob/Documents/processmakers/apache2/bin:/home/biplob/Documents/processmakers/common/bin:$PATH"
export PATH
fi
echo $LD_LIBRARY_PATH | egrep "/home/biplob/Documents/processmakers/common" > /dev/null
if [ $? -ne 0 ] ; then
LD_LIBRARY_PATH="/home/biplob/Documents/processmakers/sqlite/lib:/home/biplob/Documents/processmakers/mysql/lib:/home/biplob/Documents/processmakers/apache2/lib:/home/biplob/Documents/processmakers/common/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH
fi

TERMINFO=/home/biplob/Documents/processmakers/common/share/terminfo
export TERMINFO
##### SQLITE ENV #####
			
SASL_CONF_PATH=/home/biplob/Documents/processmakers/common/etc
export SASL_CONF_PATH
SASL_PATH=/home/biplob/Documents/processmakers/common/lib/sasl2 
export SASL_PATH
LDAPCONF=/home/biplob/Documents/processmakers/common/etc/openldap/ldap.conf
export LDAPCONF
##### GHOSTSCRIPT ENV #####
GS_LIB="/home/biplob/Documents/processmakers/common/share/ghostscript/fonts"
export GS_LIB
##### IMAGEMAGICK ENV #####
MAGICK_HOME="/home/biplob/Documents/processmakers/common"
export MAGICK_HOME

MAGICK_CONFIGURE_PATH="/home/biplob/Documents/processmakers/common/lib/ImageMagick-6.9.8/config-Q16:/home/biplob/Documents/processmakers/common/"
export MAGICK_CONFIGURE_PATH

MAGICK_CODER_MODULE_PATH="/home/biplob/Documents/processmakers/common/lib/ImageMagick-6.9.8/modules-Q16/coders"
export MAGICK_CODER_MODULE_PATH

##### FONTCONFIG ENV #####
FONTCONFIG_PATH="/home/biplob/Documents/processmakers/common/etc/fonts"
export FONTCONFIG_PATH
##### PHP ENV #####
PHP_PATH=/home/biplob/Documents/processmakers/php/bin/php
COMPOSER_HOME=/home/biplob/Documents/processmakers/php/composer
export PHP_PATH
export COMPOSER_HOME
##### MYSQL ENV #####

##### APACHE ENV #####

##### CURL ENV #####
CURL_CA_BUNDLE=/home/biplob/Documents/processmakers/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/home/biplob/Documents/processmakers/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/home/biplob/Documents/processmakers/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/home/biplob/Documents/processmakers/common/lib/engines
export OPENSSL_ENGINES


. /home/biplob/Documents/processmakers/scripts/build-setenv.sh
